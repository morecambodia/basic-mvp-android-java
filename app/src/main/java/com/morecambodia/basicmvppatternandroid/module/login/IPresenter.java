package com.morecambodia.basicmvppatternandroid.module.login;

public interface IPresenter<ViewT> {

    void onViewActive(ViewT view);

    void onViewInactive();

    void onInitView();
}
