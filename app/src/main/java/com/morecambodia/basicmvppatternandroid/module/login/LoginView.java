package com.morecambodia.basicmvppatternandroid.module.login;

import android.content.Context;

public interface  LoginView {

    interface View extends ILoginView {

        void showSuccess(String message);

        void showError(String message);
    }

    interface Presenter extends IPresenter {
        void onLogin(Context context, String username, String password);
    }
}
