package com.morecambodia.basicmvppatternandroid.module.login;

public interface  ILoginView {
    void showToastMessage(String message);

    void setProgressBar(boolean show);

    void initView();
}
