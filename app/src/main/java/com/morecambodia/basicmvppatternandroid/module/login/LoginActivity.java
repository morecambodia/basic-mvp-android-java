package com.morecambodia.basicmvppatternandroid.module.login;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.morecambodia.basicmvppatternandroid.R;

public class LoginActivity extends Activity implements LoginView.View, View.OnClickListener {
    private LoginPresenter loginPresenter;

    private EditText mEdtUsername;
    private EditText mEdtPassword;
    private TextView mMessage;
    private Button mBtnLogin;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loginPresenter = new LoginPresenter(this);
        loginPresenter.onInitView();
    }

    @Override
    public void showToastMessage(String message) {

    }

    @Override
    public void setProgressBar(boolean show) {

    }

    @Override
    public void initView() {
        setContentView(R.layout.activity_login);
        mBtnLogin = findViewById(R.id.button_login);
        mBtnLogin.setOnClickListener(this);
        mEdtUsername = findViewById(R.id.edtext_username);
        mEdtPassword = findViewById(R.id.edttext_password);
        mMessage = findViewById(R.id.textview_message);
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void showSuccess(String message) {
        mMessage.setText(message);
    }

    @Override
    public void showError(String message) {
        mMessage.setText(message);
    }

    @Override
    public void onClick(View v) {
        if (v == mBtnLogin) {
            loginPresenter.onLogin(this, mEdtUsername.getText().toString(), mEdtPassword.getText().toString());
        }
    }
}
